//
//  ViewController.swift
//  LocalNotifications
//
//  Created by Juan Pa on 10/1/18.
//  Copyright © 2018 Juan Pa. All rights reserved.
//

import UIKit
import UserNotifications

class ViewController: UIViewController {

    @IBOutlet weak var textField: UITextField!
    @IBOutlet weak var labelText: UILabel!
    
    var notificationMessage = "El texto: "
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Para recuperar
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. Acceder al valor por medio del KEY
        
        labelText.text = defaults.object(forKey: "label") as? String
        
        UNUserNotificationCenter.current().requestAuthorization(options: [.badge, .sound, .alert]) { (granted, error) in
            
        }
        
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


    @IBAction func buttonSave(_ sender: Any) {
        
        labelText.text = textField.text
        
        //Para guardar
        //1. Instanciar UserDefaults
        let defaults = UserDefaults.standard
        
        //2. Guardar variables en defaults
        // Int, Bool, Float, Double, String, Object!
        
        defaults.set(textField.text, forKey: "label")
        
        notificationMessage += labelText.text!
        
        sendNotification()
        
    }
    
    func sendNotification(){
        
        notificationMessage += labelText.text!
        
        //1. Authorization Request (está en DidLoad)
        //2. Crear contenido de la notificación
        
        let content = UNMutableNotificationContent()
        
        content.title = "Nofication Title"
        content.subtitle = "Notification Subtitle"
        content.body = notificationMessage
        
        //2.1 Crear acciones
        
        let repeatAction = UNNotificationAction(identifier: "repeat", title: "Repetir", options: [])
        
        let changeMessage = UNNotificationAction(identifier: "change", title: "Cambiar mensaje", options: [])
        
        // 2.2 Agregar acciones a un UNNotificationCategory
        
        let category = UNNotificationCategory(identifier: "actionsCat", actions: [repeatAction, changeMessage], intentIdentifiers: [], options: [])
        
        //2.3 Agregar el category al content
        
        content.categoryIdentifier = "actionsCat"
        
        //2.4 Agregar category al UNUserNotificationCenter
        UNUserNotificationCenter.current().setNotificationCategories([category])
        
        
        //3. Definir un trigger
        
        let trigger = UNTimeIntervalNotificationTrigger(timeInterval: 10, repeats: false)
        
        //4. Definir un identificador para la notificación
        
        let identifier = "Notification"
        
        //5. Crear un request
        
        let notificationRequest = UNNotificationRequest(identifier: identifier, content: content, trigger: trigger)
        
        //6. Añadir el request al UNUserNotificationCenter
        
        UNUserNotificationCenter.current().add(notificationRequest) { (error) in
            
            
        }
        
    }
    
}

